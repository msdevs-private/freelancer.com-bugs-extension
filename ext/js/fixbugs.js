function showCountries(){
	jq(".flag-icon").each(function(index, val) {
		 var $this = jq(this);
		 if($this.data('msdevs_set'))
		 	return;
		 
		 var country = $this.attr("alt");
		 if (country && country !== "") {
		 	$this.after("<div class='clear'>" + country + " ✔</div>");
		 }
		 $this.data('msdevs_set',true);
	});
}

function showBuyerUrl(){
	var $buyers = jq(".buyerWrapper");
	if ($buyers.length > 0) {
		$buyers.each(function(index, el) {
			var $this = jq(this);

			if($this.data("msdevs_set"))
				return;

			$this.prepend("<h3><a href='" + project.buyer.url + "'>" + project.buyer.username + " ✔</a></h3>");
			$this.data('msdevs_set', true);
		});
	};
}

function enableEmployerOverview () {
	var target = jq("#failBubbleEmployerOverview").next();
	jq('<a href="javascript:showEmployerView()" btntype="buyer" class="ns_toggle-btn ns_btn-right ns_left">Employer ✔</a>').insertBefore(target);
	jq("#failBubbleEmployerOverview").remove();	
}

var intervalHandle = setInterval(function() {

if (jq) {

	setInterval(function(){
	
		showCountries();
		showBuyerUrl();
		enableEmployerOverview();

	},1000);	
	clearInterval(intervalHandle);

	
};

}, 50);