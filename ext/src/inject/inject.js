chrome.extension.sendMessage({}, function(response) {
	var readyStateCheckInterval = setInterval(function() {
	if (document.readyState === "complete") {
		clearInterval(readyStateCheckInterval);

		// ----------------------------------------------------------
		// This part of the script triggers when page is done loading
		console.log("Freelancer.com bug fixes loaded");
		// ----------------------------------------------------------

		//Add JQuery
		var jqueryScript = document.createElement("script");
		jqueryScript.src = chrome.extension.getURL("js/jquery-2.1.1.min.js");
		document.documentElement.appendChild(jqueryScript);

		var jqueryScript = document.createElement("script");
		jqueryScript.src = chrome.extension.getURL("js/fixbugs.js");
		document.documentElement.appendChild(jqueryScript);
	}	
	}, 10);
});